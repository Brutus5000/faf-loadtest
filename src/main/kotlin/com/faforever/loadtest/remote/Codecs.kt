package com.faforever.loadtest.remote

import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import reactor.core.publisher.Flux
import java.nio.ByteBuffer
import java.nio.charset.Charset

interface Codec<T> {
    fun decode(byteBuffer: ByteBuf): Flux<String>
    fun encode(data: T): ByteArray
}

class StringCodec(val charset: Charset) : Codec<String> {
    override fun decode(byteBuffer: ByteBuf): Flux<String> {
        return Flux.just(byteBuffer.toString(charset))
    }

    override fun encode(data: String): ByteArray {
        return data.toByteArray(charset)
    }
}

// QDatastream is framed by a block, with a leading Integer stating the size of the block
// Inside the block can be n QStrings, always starting with a leading Integer stating the size of the string
class QDatastreamCodec : Codec<String> {
    var blockSize: Int? = null
    var blockBuffer: ByteBuf? = null


    override fun decode(byteBuffer: ByteBuf): Flux<String> {
        val isNewMessage = blockSize == null
        mergeBuffers(byteBuffer)

        return if (blockSize!! <= blockBuffer!!.readableBytes()) {
            decodeMessage(blockBuffer!!, isNewMessage)
        } else {
            Flux.empty()
        }
    }

    /**
     * If the message size exceeds the buffer size, we need to build a buffer for the whole message
     */
    private fun mergeBuffers(byteBuffer: ByteBuf) {
        if (blockSize == null) {
            blockSize = byteBuffer.getInt(0)
            blockBuffer = byteBuffer.copy()
        } else {
            blockBuffer = Unpooled.copiedBuffer(blockBuffer, byteBuffer)
        }
    }

    private fun decodeMessage(byteBuffer: ByteBuf, isNewMessage: Boolean): Flux<String> {
        return Flux.generate(
                // The 1st QString starts after the 4 bytes of block size
                { 4 },
                { nextStringIndex, sink ->
                    if (nextStringIndex < blockSize!!) {
                        val qstringSize = byteBuffer.getInt(nextStringIndex)
                        val string = byteBuffer.copy(nextStringIndex + 4, qstringSize).toString(Charsets.UTF_16BE)

                        sink.next(string)
                        nextStringIndex + qstringSize + 4
                    } else {
                        sink.complete()
                        blockSize = null
                        blockBuffer = null
                        nextStringIndex
                    }
                }
        )
    }

    override fun encode(data: String): ByteArray {
        val byteArray = data.toByteArray(Charsets.UTF_16BE)
        return ByteBuffer.allocate(Int.SIZE_BYTES * 2 + byteArray.size)
                // QDatastream block header
                .putInt(byteArray.size + Int.SIZE_BYTES)
                // QString header
                .putInt(byteArray.size)
                // Actual data
                .put(byteArray)
                .array()
    }
}