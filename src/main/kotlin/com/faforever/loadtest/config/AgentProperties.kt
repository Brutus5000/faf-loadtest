package com.faforever.loadtest.config

import org.jetbrains.annotations.NotNull
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank


@ConfigurationProperties(prefix = "loadtest")
@ConstructorBinding
@Validated
data class AgentProperties(
        @NotBlank
        val host: String,

        @NotNull
        val port: Int,

        @NotNull
        val accounts: List<Account>,

        /**
         * If true, will use simple strings instead of QDatastream protocol.
         *
         * For simple usage with an echo server like:
         * `docker run --rm -it -e TCP_PORT=2701 -p 2701:2701 cjimti/go-echo}`
         */
        @NotNull
        val debugMode: Boolean
)

data class Account(
        val user: String,
        val password: String
)