package com.faforever.loadtest.agent

import com.faforever.loadtest.config.AgentProperties
import com.faforever.loadtest.remote.*
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import reactor.core.publisher.FluxSink
import reactor.core.publisher.UnicastProcessor
import reactor.core.publisher.toFlux
import reactor.netty.tcp.TcpClient
import java.time.Duration


@Component
class AgentComposer(properties: AgentProperties, mapper: ObjectMapper) {
    companion object {
        val log: Logger = LoggerFactory.getLogger(Agent::class.java)
    }

    private val agents: MutableMap<String, Agent> = HashMap()

    init {
        // TODO: Do this in the configuration package
        mapper.registerModule(KotlinModule())

        val delaySeconds = 1

        log.info("Ramping up ${properties.accounts.size} users with $delaySeconds seconds delay")

        properties.accounts.toFlux()
                .delayElements(Duration.ofSeconds(1))
                .subscribe(
                        {
                            val codec = when (properties.debugMode) {
                                true -> StringCodec(Charsets.UTF_8)
                                false -> QDatastreamCodec()
                            }

                            log.info("Starting Agent '${it.user}'")

                            Agent(codec, mapper, properties, it.user, it.password).apply {
                                agents[user] = this
                                start()
                            }
                        },
                        { log.error("Ramp up of users failed!", it) },
                        { log.info("Ramp up of users complete") }
                )
    }
}

class Agent(val codec: Codec<String>, val mapper: ObjectMapper, val properties: AgentProperties, val user: String, val password: String) {
    companion object {
        val log: Logger = LoggerFactory.getLogger(Agent::class.java)
    }

    private val outboundSink: FluxSink<String>
    private var currentState: State = WaitForSessionState(GlobalContext(user, password))

    init {
        val processor = UnicastProcessor.create<String>()
        outboundSink = processor.sink(FluxSink.OverflowStrategy.ERROR)

        val connection = TcpClient.create()
                .host(properties.host)
                .port(properties.port)
                .handle { inbound, outbound ->
                    val inboundMono = inbound.receive()
                            // decode QDataStream block
                            .flatMap(codec::decode)
                            .doOnNext { log.debug("Agent '$user' - Inbound message: {}", it) }
                            .filter {
                                if (it == "PING") {
                                    outboundSink.next("PONG")
                                    false
                                } else true
                            }
                            .map { mapper.readValue(it, ServerMessage::class.java) }
                            .doOnNext {
                                val (clientMessage, newState) = currentState.transition(it)
                                currentState = newState
                                clientMessage?.run(this::send)
                            }
                            .doOnError { log.error("Agent '$user' - Error during read", it) }
                            .doOnComplete { log.info("Agent '$user' - inbound channel closed") }
                            .doFinally { log.info("Agent '$user' - inbound channel finally") }
                            .then()

                    val outboundMono = outbound.sendByteArray(
                            processor
                                    .doOnNext { log.debug("Agent '$user' - Outbound message: {}", it) }
                                    .doOnError { log.error("Agent '$user' - Error during read", it) }
                                    .doOnComplete { log.error("Agent '$user' - outbound channel closed") }
                                    .doFinally { log.info("Agent '$user' - outbound channel finally") }
                                    .map(codec::encode)
                    ).then()

                    inboundMono.mergeWith(outboundMono)
                }
                .connect()
                .subscribe()
    }

    fun start() {
        send(SessionRequest())
    }

    fun send(message: ClientMessage) {
        outboundSink.next(mapper.writeValueAsString(message))
    }
}