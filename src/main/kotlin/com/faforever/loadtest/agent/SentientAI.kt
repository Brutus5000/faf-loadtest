package com.faforever.loadtest.agent

import com.faforever.loadtest.remote.*
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.nio.file.Paths
import java.security.MessageDigest
import java.util.*

interface State {
    fun transition(message: ServerMessage): Pair<ClientMessage?, State>
}

data class GlobalContext(
        val username: String,
        val password: String,
        val session: Long? = null
)

class InitialState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return Pair(SessionRequest(), WaitForSessionState(context))
    }
}

class TerminatedState() : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return Pair(null, this)
    }
}

class TemplateState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return when (message) {
            else -> Pair(null, this)
        }
    }
}

class WaitForSessionState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return when (message) {
            is SessionResponse -> Pair(
                    LoginRequest(context.username, sha256(context.password), message.session, generateUid(message.session, Paths.get("faf-uid")), randomIp()),
                    WaitForLoginState(context.copy(session = message.session)
                    )
            )
            else -> Pair(null, this)
        }
    }

    private fun randomIp(): String {
        return "10.${(0..254).random()}.${(0..254).random()}.${(0..254).random()})"
    }

    private fun sha256(password: String): String {
        val md = MessageDigest.getInstance("SHA-256")
        val digest = md.digest(password.toByteArray())
        return digest.fold("", { str, it -> str + "%02x".format(it) })
    }

    @Throws(IOException::class)
    private fun execAndGetOutput(cmd: String): String {
        val scanner = Scanner(
                Runtime.getRuntime().exec(cmd).inputStream, StandardCharsets.UTF_8.name()
        ).useDelimiter("\\A")
        return if (scanner.hasNext()) scanner.next().trim { it <= ' ' } else ""
    }

    @Throws(IOException::class)
    private fun generateUid(sessionId: Long, logFile: Path): String {
        val uidDir = System.getProperty("nativeDir", "lib")
        val uidPath = Paths.get(uidDir).resolve("faf-uid")
        return execAndGetOutput(String.format("%s %s", uidPath.toAbsolutePath(), sessionId))
    }
}

class WaitForLoginState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return when (message) {
            is InvalidResponse -> throw IllegalStateException("Login failed")
            is LoginResponse -> Pair(null, IdleState(context))
            else -> Pair(null, this)
        }
    }
}

class IdleState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return when (message) {
            is MatchmakerInfo ->
                Pair(
                        HostGameRequest(
                                "adaptive_lone_eagle.v0006",
                                "${context.username}'s game",
                                "faf",
                                BooleanArray(0),
                                GameAccess.PUBLIC,
                                1,
                                null,
                                GameVisibility.PUBLIC
                        ),
                        WaitAsGameHostState(context)
                )
//                Pair(
//                    SearchLadder1v1Request(faction = "uef"),
//                    WaitForLadderMatchState(context)
//            )
            else -> Pair(null, this)
        }
    }
}

class WaitForLadderMatchState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return when (message) {
            else -> Pair(null, this)
        }
    }
}

class WaitAsGameHostState(private val context: GlobalContext) : State {
    override fun transition(message: ServerMessage): Pair<ClientMessage?, State> {
        return when (message) {
            else -> Pair(null, this)
        }
    }
}