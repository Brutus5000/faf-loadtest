package com.faforever.loadtest

import com.faforever.loadtest.config.AgentProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(AgentProperties::class)
class LoadtestApplication

fun main(args: Array<String>) {
    runApplication<LoadtestApplication>(*args)
}
